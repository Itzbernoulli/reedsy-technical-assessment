module CoreExtensions
  module BigDecimal
    def to_two_dp
      '%.2f' % self
    end
  end
end