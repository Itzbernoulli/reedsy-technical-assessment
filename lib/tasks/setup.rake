namespace :setup do
  
  desc 'Setup the database with the sample data'
  task db: ['db:drop', 'db:create', 'db:migrate', 'db:seed'] do
    puts 'Database Setup Complete'
  end
end
