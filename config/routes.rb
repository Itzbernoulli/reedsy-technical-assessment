Rails.application.routes.draw do
  resources :products, only: %i[index update] do
    collection do
      get :list_price
    end
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
