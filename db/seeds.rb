# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Product.delete_all

codes = %i[MUG TSHIRT HOODIE]
names = ['Reedsy Mug', 'Reedsy T-shirt', 'Reedsy Hoodie']
prices = [6.00, 15.00, 20.00]

(0..2).each do |index|
  Product.create!(
    code: codes[index],
    name: names[index],
    price: BigDecimal(prices[index], 2)
  )
end

puts 'Product creation successful'
