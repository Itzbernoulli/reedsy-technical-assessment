require "test_helper"

class ListPriceServiceTest < ActiveSupport::TestCase

  def test_items_not_needing_discounts
    items = '1 MUG, 1 TSHIRT, 1 HOODIE'
    response = ListPriceService.new(items).execute
    assert_equal(41.00, response[:total])
  end

  def test_mugs_needing_discounts
    items = '10 MUG, 1 TSHIRT'
    response = ListPriceService.new(items).execute
    assert_equal(73.80, response[:total])
  end

  def test_tshirts_needing_discounts
    items = '200 MUG, 4 TSHIRT, 1 HOODIE'
    response = ListPriceService.new(items).execute
    assert_equal(902.00, response[:total])
  end

  def test_mugs_and_tshirts_needing_discounts
    items = '45 MUG, 3 TSHIRT'
    response = ListPriceService.new(items).execute
    assert_equal(279.90, response[:total])
  end
end