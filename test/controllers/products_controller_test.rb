require "test_helper"

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url, as: :json
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: { product: { code: @product.code, name: @product.name, price: @product.price } }, as: :json
    assert_response :success
  end
end
