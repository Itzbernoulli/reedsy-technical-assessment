# Reedsy technical assessment

This project is written in Ruby 3.1.0 and Rails 7.0.2 and uses a SQLite database

## Setup Instructions

After pulling the project, 

```rails setup:db```

This will setup the database to be ready for use.

To make sure all tests pass run ```rails test```

Next run ```rails s``` to get the project started on the default port 3000 and make calls via a rest client or curl.

## Sample Requests
### All Products
Sample Request: 
```curl http://localhost:3000/products```

Get all products in the database


Sample Response:

[
    {
        "id": 23,
        "code": "MUG",
        "name": "Reedsy Mug",
        "price": "6.00",
        "created_at": "2022-05-08T21:48:57.823Z",
        "updated_at": "2022-05-08T21:48:57.823Z"
    },
    {
        "id": 24,
        "code": "TSHIRT",
        "name": "Reedsy T-shirt",
        "price": "15.00",
        "created_at": "2022-05-08T21:48:57.826Z",
        "updated_at": "2022-05-08T21:48:57.826Z"
    },
    {
        "id": 25,
        "code": "HOODIE",
        "name": "Reedsy Hoodie",
        "price": "20.00",
        "created_at": "2022-05-08T21:48:57.828Z",
        "updated_at": "2022-05-08T21:48:57.828Z"
    }
]
```




### Update product

Update a specific product
Sample Request:
```curl -d '{ "product" :{"price":"20"}}' -H 'Content-Type: application/json' -X PUT http://localhost:3000/products/23```

```
{
    "id": 23,
    "code": "MUG",
    "name": "Reedsy Mug",
    "price": "20.00",
    "created_at": "2022-05-08T21:48:57.823Z",
    "updated_at": "2022-05-09T14:05:55.770Z"
}
```
### Check price of a given list of items

Update a specific product
Sample Request:

```curl -d '{ "items": "200 MUG, 4 TSHIRT, 1 HOODIE"}' -H 'Content-Type: application/json' -X GET http://localhost:3000/products/list_price```

```
{
    "items": "200 MUG, 4 TSHIRT, 1 HOODIE",
    "total": "902.00"
}
```
