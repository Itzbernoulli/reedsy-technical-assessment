class Product < ApplicationRecord
  validates_presence_of :code, :name
  validates_numericality_of :price

end
