class ProductsController < ApplicationController
  before_action :set_product, only: %i[show update destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    if @product.update(product_params)
      render :show, status: :ok, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def list_price
    render json: ListPriceService.new(params[:items]).execute
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def product_params
    params.require(:product).permit(:price)
  end
end
