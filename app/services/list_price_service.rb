class ListPriceService
  attr_reader :items

  def initialize(items)
    @items = items
  end

  def execute
    sum = 0
    @items.split(',').each do |pair|
      quantity, code = pair.split(' ')

      price = Product.find_by_code(code).price
      sum += apply_discount(quantity.to_i, price.to_d, code)
    end
    OpenStruct.new(items: @items, total: sum.to_two_dp).table
  end

  def apply_discount(quantity, price, code)
    discounted_price = quantity * price
    case code
    when 'TSHIRT'
      discounted_price -= 0.3 * discounted_price if quantity >= 3
    when 'MUG'
      return discounted_price if quantity < 10

      return discounted_price -= 0.3 * discounted_price if quantity > 140

      discounted_price -= 0.02 * quantity.digits.last * discounted_price
    end
    discounted_price
  end
end
