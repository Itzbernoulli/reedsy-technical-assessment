json.extract! product, :id, :code, :name, :price, :created_at, :updated_at
json.price product.price.to_two_dp